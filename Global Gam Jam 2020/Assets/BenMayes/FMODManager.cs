﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static FMODManager _instance;
    public static FMODManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<FMODManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("FMODManager");
                    _instance = container.AddComponent<FMODManager>();
                }

            }
            return _instance;
        }
    }
    #endregion

    FMOD.Studio.EventInstance RiverFlow2;
    private float riverflowvalue;
    // Start is called before the first frame update
    void Start()
    {
        RiverFlow2 = FMODUnity.RuntimeManager.CreateInstance("event:/RiverFlow2");
        RiverFlow2.start();
    }




    public void UpdateWaterFlowRateParameter()
    {
        //== GameManager.Instance.damNodeManager.flowRate;
        

        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(GameManager.Instance.currentTotalFlow);
        RiverFlow2.getParameterByName("WaterIntensity", out riverflowvalue);
        //Debug.Log("riverflowvalue:" + riverflowvalue);
        RiverFlow2.setParameterByName("WaterIntensity", GameManager.Instance.currentTotalFlow);
    }



    public class ManualExample : MonoBehaviour
    {

        private FMOD.Studio.EventInstance instance;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                instance = FMODUnity.RuntimeManager.CreateInstance("event:/LogGrowWide");
                instance.start();
                instance.release();
            }
        }
    }


}
