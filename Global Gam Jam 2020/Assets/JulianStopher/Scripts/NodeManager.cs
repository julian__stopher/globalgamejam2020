﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
public class NodeManager : MonoBehaviour
{

    public GameObject node;

    private int worldWidth = 16;
    private int worldHeight = 9;

    public Camera mainCamera;
    private int spawnTally;

    public List<GameObject> activeNodes = new List<GameObject>();
    public List<GameObject> inactiveNodes = new List<GameObject>();
    public List<Material> damNodeMaterials = new List<Material>();

    private int totalNodeTally = 144;

    public bool isManagingDam = false;

    public float flowRate = 0f;

    void Start()
    {
        if (node.GetComponent<Node>().isNodeWater)
        {
            CreateNodes(0);
        }
        else
        {
            CreateNodes(1);
        }
        
    }

    private void Update()
    {

        if (isManagingDam)

        {
            CalculateFlowRate();
        }
       
    }

   public void CalculateFlowRate()
    {

        flowRate = 1- activeNodes.Count /totalNodeTally;
    }

    public void  CreateNodes(int nodeType)
    {
        for (int x = 0; x < worldWidth; x++)
        {
     

            for (int y = 0; y < worldHeight; y++)
            {
          

                GameObject tempNode = Instantiate(node, Vector3.zero, node.transform.rotation) as GameObject;
                tempNode.GetComponent<Node>().nodeID = spawnTally;
                tempNode.GetComponent<Node>().mainCam = mainCamera;
                tempNode.GetComponent<MeshRenderer>().material = damNodeMaterials[Random.Range(0,damNodeMaterials.Count)];
                tempNode.transform.parent = transform;
                tempNode.transform.localPosition = new Vector3(x, y, 0);
                if(nodeType == 0)
                {
                    tempNode.name = "waterNode" + spawnTally;
                }
                else if(nodeType == 1)
                {
                    tempNode.name = "damNode" + spawnTally;
                }

                activeNodes.Add(tempNode);
                spawnTally++;
            }
        }
        spawnTally = 0;
    }

    public void HideDamNode()
    {
        //  get a random index from the active list
        int randomIndex = Random.Range(0, activeNodes.Count);
        //hide the object
        activeNodes[randomIndex].GetComponent<MeshRenderer>().enabled = false;
        //add to inactive list
        inactiveNodes.Add(activeNodes[randomIndex]);
        //remove from active list.
        activeNodes.Remove(activeNodes[randomIndex]);

        FMODUnity.RuntimeManager.PlayOneShot("event:/Dam Hole");
        //FMODManager.Instance.PlaySound("BustDam");
    }


    public void ShowDamNode(int specifiedNodeID)
    {
        int damNodesFilled = 0;
        //show the object
        foreach (GameObject node in inactiveNodes.ToList())
        {
            if (node.GetComponent<Node>().nodeID == specifiedNodeID)
            {
                node.GetComponent<MeshRenderer>().enabled = true;
                //remove from inactive list
                inactiveNodes.Remove(node);
                //add to active list.
                activeNodes.Add(node);
                damNodesFilled++;
            }
        }


        float bonus = damNodesFilled * damNodesFilled * damNodesFilled;

        GameManager.Instance.GivePlayerFlowBonus(bonus);
    }


}