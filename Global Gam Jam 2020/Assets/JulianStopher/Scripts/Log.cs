﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log : MonoBehaviour
{

    #region variables
    /// <summary>
    ///list of currently missing dam bits, check if renderer is disabled on the object in the list.
    /// </summary>
    public List<GameObject> currentOverlappingEmptyLogSpaces = new List<GameObject>();

    public List<Sprite> logSprites = new List<Sprite>();
    #endregion


    private void Start()
    {
        RandomiseLogSprite();
    }

    public void RandomiseLogSprite()
    {
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = logSprites[Random.Range(0, logSprites.Count)];
    }

    public void PlaceLog()
    {

        FMODUnity.RuntimeManager.PlayOneShot("event:/PlaceLog");

        for(int i = 0; i < currentOverlappingEmptyLogSpaces.Count; i++)

        {
            int currentNodeID = currentOverlappingEmptyLogSpaces[i].GetComponent<Node>().nodeID;
            GameManager.Instance.damNodeManager.ShowDamNode(currentNodeID);
        }

        GameManager.Instance.activeLog = null;
        GameManager.Instance.SpawnLog();
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider collidingObject)
    {
        //if its part of the dam
        if (!collidingObject.gameObject.GetComponent<Node>().isNodeWater)
        {
            //if the renderer is disabled (empty space)
            if(!collidingObject.gameObject.GetComponent<Renderer>().enabled)
                currentOverlappingEmptyLogSpaces.Add(collidingObject.gameObject);
        }
        
    }

    private void OnTriggerExit(Collider collidingObject)
    {
        //if its part of the dam
        if (!collidingObject.gameObject.GetComponent<Node>().isNodeWater)
        {
            //if the renderer is disabled (empty space)
            if (!collidingObject.gameObject.GetComponent<Renderer>().enabled)
                currentOverlappingEmptyLogSpaces.Remove(collidingObject.gameObject);
        }
    }
}
