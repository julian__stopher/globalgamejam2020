﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCalculate : MonoBehaviour
{
    public MeshFilter mesh;

    Vector3[] vertexMesh;

    public Camera cam;

    int count;

    [SerializeField]
    public Plane[] planes;

    void Start()
    {
        vertexMesh = mesh.mesh.vertices;
    }

    void Update()
    {
        planes = GeometryUtility.CalculateFrustumPlanes(cam);
        count = 0;
        foreach (Plane plane in planes)
        {
            foreach (Vector3 vertex in vertexMesh)
            {
                if (!(plane.GetDistanceToPoint(mesh.transform.TransformPoint(vertex)) < 0))
                {
                    count++;
                }
            }
        }
        print(count);
    }
}
