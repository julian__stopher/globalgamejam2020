﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("GameManager");
                    _instance = container.AddComponent<GameManager>();
                }

            }
            return _instance;
        }
    }
    #endregion


    #region variables

    /// <summary>
    /// meter will start at zero and go up
    /// </summary>
    private float totalFlowThreshold = 100f;
    public float currentflowRate;
    public float currentTotalFlow;

    private int currentWaterNodesVisible;

    private float currentDamBustTime = 0f;
    private float damBustTimeThreshold = 2f;
    private bool startTimer = true;
    public NodeManager damNodeManager;
    public Image totalFlowImage;
    public Image energyBar;

    public Transform logSpawnPoint;
    public GameObject logPrefab;
    public GameObject activeLog;
    public Text scoreText;
    private float logBuildSpeed;
  
    private int currentDifficultyLevel = 0;
    private float currentGameTime;
    private float gameTimeWinThreshold = 88f;
    private float level2Threshold = 60f;
    private float level3Threshold = 80f;
    private bool startGameTimer = true;
    private bool level2Reached = false;
    private bool level3Reached = false;
    private bool horizontalBuildActive = false;
    private bool verticalBuildActive = false;
    private float bonusModifier = 1f;
    private float currentEnergyAmount;
    private float energyThreshold = 100f;
    private float horizontalBuildCost = 1f;
    private float verticalBuildCost = 1f;
    private float flowDifficultyModifier = 1f;
    private float drainDifficultyModifier = 1f;

    private float playerScore = 0f;
    private bool gameFinished = false;
    public Animator beaverAnimator;
    #endregion


    private void OnEnable()
    {
        SceneManager.sceneLoaded += CheckCurrentScene;
    }

    public void CheckCurrentScene(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "LoseScreen" || scene.name == "WinScreen")
        {
            GameObject.FindGameObjectWithTag("SCORETEXT").GetComponent<Text>().text = Mathf.Round(playerScore).ToString();
        }

        if(scene.name == "LoseScreen")
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/EndGame");
        }
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        currentEnergyAmount = energyThreshold;
        SpawnLog();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }


    void Update()
    {
        if (gameFinished)
            return;
        ManageFlowRate();
        ManageDamBustTimer();
        ManagePlayerInput();
        ManageGameLose();
        ManageGameTimer();
        ManageEnergy();
        ManageScoreIncrease();
    }

    public void IncreaseDifficulty()
    {
        currentDifficultyLevel++;

        switch (currentDifficultyLevel)
        {
            case 1:

                damBustTimeThreshold = 1f;
                bonusModifier = .7f;
                flowDifficultyModifier = 2f;
                drainDifficultyModifier = .7f;
                break;

            case 2:

                damBustTimeThreshold = 0.5f;
                bonusModifier = .5f;
                flowDifficultyModifier = 4f;
                drainDifficultyModifier = .6f;
                break;

        }
    }

    public void ManageScoreIncrease()
    {
        playerScore += Time.deltaTime * 100;
        scoreText.text = Mathf.Round(playerScore).ToString();
    }
    public void ManageGameTimer()
    {
        if (!startGameTimer)
            return;
       // Debug.Log("current game time"+ currentGameTime);

        if (currentGameTime < gameTimeWinThreshold)
        {
            currentGameTime += Time.deltaTime;
        }
        else
        {
            currentGameTime = 0;
            startGameTimer = false;
            ManageGameWin();
        }
        if (currentGameTime >= level2Threshold && currentGameTime < level3Threshold && !level2Reached)
        {
            Debug.Log("level2reached");
            level2Reached = true;
            IncreaseDifficulty();
        }
        else if (currentGameTime >= level3Threshold && currentGameTime < gameTimeWinThreshold && !level3Reached)
        {
            Debug.Log("level3reached");
            level3Reached = true;
            IncreaseDifficulty();
        }
    
    }


    public void ManageFlowRate()
    {

        if (damNodeManager.inactiveNodes.Count > 0)
        {
            currentTotalFlow += Time.deltaTime * damNodeManager.flowRate * 3 * flowDifficultyModifier;
        }
        else
        {
            if (currentTotalFlow <= 0)
                return;
            currentTotalFlow -= Time.deltaTime * 10 * flowDifficultyModifier;
        }
        totalFlowImage.fillAmount = currentTotalFlow / totalFlowThreshold;
       // Debug.Log("total flow" + currentTotalFlow);
    }

    public void ManageGameWin()
    {
        gameFinished = true;
        SceneManager.LoadScene("WinScreen");
    }



    public void ManageGameLose()
    {
        if((currentTotalFlow / totalFlowThreshold) >= 1)
        {
            gameFinished = true;
            SceneManager.LoadScene("LoseScreen");
        }
    }

    public void ManageDamBustTimer()
    {
        if (!startTimer)
            return;

        if (currentDamBustTime < damBustTimeThreshold)
        {
            currentDamBustTime += Time.deltaTime;
        }
        else
        {
            currentDamBustTime = 0;
            HideDamNodes();
        }

    }


    public void HideDamNodes()
    {
        damNodeManager.HideDamNode();

    }

    /*
     get a random index
     hide the object or delete it? Hide, because you'd have to replace that specific node in that specific position.
     remove from the list? 
     the renderer of the dam node needs to be disabled and then the node needs to be moved to an inactive list
    how to detect big log on top (plug hole)? 
    collision detection, so when the log collides with dam node, get id, take off inactive list and move to active list, then enable renderer

    
     //with the dam cubes, you wont even need to do raycast, because you can calculate water flow by checking how much of the dam is missing
     // certain times you will want to remove nodes at the same time

/// variables (destroy rate interval, number of nodes to destroy etc)
     
     */


    //for making the logs 
    //current beaver object
    //scale by x with 1 button
    //scale with y by 1 button
    //move the log with arrow keys
    //set the log with Enter



    public void SpawnLog()
    {
        if (!activeLog || activeLog == null)
        {
            GameObject newLog = Instantiate(logPrefab, logSpawnPoint);
            newLog.transform.position = logSpawnPoint.position;
            activeLog = newLog;
        }
    }
    public void ManagePlayerInput()
    {

        if (activeLog && activeLog != null)
        {
            ManageLogMovement();
            ManageLogScale();
            ManageLogPlacement();

        }
        //   if(Input.GetKeyDown(KeyCode.Space))
    }


    public void ManageEnergy()
    {
       // Debug.Log(currentEnergyAmount);
        energyBar.fillAmount = currentEnergyAmount / energyThreshold;

        if (currentEnergyAmount <= 0)
            {
            currentEnergyAmount = 2f;
            }

        if (horizontalBuildActive || verticalBuildActive)
        {
            if (currentEnergyAmount > 0)
            {
                currentEnergyAmount -= Time.deltaTime * horizontalBuildCost * verticalBuildCost * 8 * drainDifficultyModifier;
            }
           
            
        }

        if (!horizontalBuildActive && !verticalBuildActive)
        {
            if (currentEnergyAmount < energyThreshold)
            {
                currentEnergyAmount += Time.deltaTime * 1f;
            }
        }
    }
    public void ManageLogScale()
    {
        if (currentEnergyAmount < 5)
        {
            horizontalBuildActive = false;
            verticalBuildActive = false;
            return;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            beaverAnimator.SetBool("beaverBuilding", true);
            FMODUnity.RuntimeManager.PlayOneShot("event:/LogGrowWide");
            horizontalBuildActive = true;
            horizontalBuildCost = 2f;
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            beaverAnimator.SetBool("beaverBuilding", false);
            horizontalBuildActive = false;
            horizontalBuildCost = 1f;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            beaverAnimator.SetBool("beaverBuilding", true);
            FMODUnity.RuntimeManager.PlayOneShot("event:/LogGrowWide");
            verticalBuildActive = true;
            verticalBuildCost = 2f;
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            beaverAnimator.SetBool("beaverBuilding", false);
            verticalBuildActive = false;
            verticalBuildCost = 1f;
        }


        if (Input.GetKey(KeyCode.X))
        {
            activeLog.transform.localScale += new Vector3(1, 0, 0) * 4f * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.C))
        {
            activeLog.transform.localScale += new Vector3(0, 1, 0) * 4f * Time.deltaTime;
        }


    }

    public void ManageLogMovement() 
    {
       
        var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        activeLog.transform.position += move * 7f * Time.deltaTime;
        
    }

    public void ManageLogPlacement()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            activeLog.GetComponent<Log>().PlaceLog();
        }
    }


    public void GivePlayerFlowBonus(float bonus)
    {
        currentTotalFlow -= (5f + bonus)*bonusModifier;
        if (currentTotalFlow < 0)
            currentTotalFlow = 0.1f;
        currentEnergyAmount+= (2f + bonus) * bonusModifier * 0.8f;
        if(currentEnergyAmount > 100)
        {
            currentEnergyAmount = 100;
        }
        playerScore += 100 * bonus;
    }
}
